import { combineReducers } from 'redux';
import users from '../module/users';



// За какую часть отвечает наш module
// state.count
// Значение будет module который умеет управлять частью нашего store
export default combineReducers({
    users
});