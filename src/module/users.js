export const GET_USERS_SUCCESS = 'clientsList/GET_USERS_SUCCESS';
export const GET_USERS_ERROR = 'clientsList/GET_USERS_ERROR';
export const GET_ONE_USER = 'GET_ONE_USER';
export const FILTER_USER = 'FILTER_USER';

import "regenerator-runtime/runtime";

const initialState = {
    users: [],
    activeData: null,
    filterUser: null
};

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_ONE_USER:
            return Object.assign({}, {
                ...state,
                activeData: state.users.filter((el) => {
                    return el.general.firstName === action.name ? true : false
                })[0]
            });
        case FILTER_USER:
            return Object.assign({}, {
                ...state,
                filterUser: state.users.filter((el) => {
                    let searchValue =  el.general.firstName.toLowerCase();
                    return searchValue.indexOf(action.name) !== -1;
                })
            });
        case GET_USERS_SUCCESS:
            return Object.assign({}, {
                ...state,
                users: action.response
            });
        default:
            return state;
    }
}

export const getData = (name) => ({
    type: GET_ONE_USER,
    name
});

export const filterName = (name) => ({
    type: FILTER_USER,
    name
});


export const getUsersSuccess = (response) => ({
    type: GET_USERS_SUCCESS,
    response
});

export const getUsersError = error => ({
    type: GET_USERS_ERROR,
    error
});

export const getUsers = () => async dispatch => {
    try {
        let url = "/src/clients.json";
        fetch(url)
            .then((response) => {
                response.json().then((result) => {
                    dispatch(getUsersSuccess(result));
                })
            })
            .catch((error) => {
                console.log(error);
            });
    } catch (e) {
        console.log("eee", e);
    }
};

