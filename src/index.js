import React from "react";
import ReactDOM from 'react-dom';
import store from './store/index';
import {Provider} from 'react-redux';
import App from './componnets/containers/App/index';
import './css/style.css';


const target = document.querySelector('#container');
ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    target
);


