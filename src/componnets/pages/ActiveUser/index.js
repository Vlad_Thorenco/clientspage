import React, {Component} from "react";

class ActiveUser extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {info} = this.props;
        return (
            <div className='active-blc-user'>
                <div className="logo-active">
                    <img src={info.general.avatar} alt=""/>
                </div>
                <div className='description-user-active'>
                    <h3>Name: {info.general.firstName}</h3>
                    <h3>Surname: {info.general.lastName}</h3>
                    <p>Job: {info.job.company}</p>
                    <p>Title: {info.job.title}</p>
                    <p>email:{info.contact.email} </p>
                    <p>Phone: {info.contact.phone}</p>
                    <p>street: {info.address.street}</p>
                    <p>city: {info.address.city}</p>
                    <p>zipcode: {info.address.zipCode}</p>
                    <p>country: {info.address.country}</p>
                </div>
            </div>
        )
    };
}

export default ActiveUser;

