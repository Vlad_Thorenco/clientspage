import React, {Component} from "react";
import { getData } from '../../../module/users';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

class ItemUser extends Component {
    constructor(props) {
        super(props);
    }

    click(name) {
        // this.props.getUserData(id)
        this.props.getData(name);
        // console.log(this.props.users.users);
    }

    render() {
        const {name, job, avatar, key} = this.props;
        return (
            <li className='item-user' key={key} onClick={() => this.click(this.props.name.firstName)}>
                <div className='card-users'>
                    <p>Name: {name.firstName}</p>
                    <p>Surname: {name.lastName}</p>
                    <p>Company: {job.company}</p>
                    <div className="logo-users">
                        <img src={avatar.avatar} alt="user-img" />
                    </div>
                </div>
            </li>
        )
    };
}
const mapStateToProps = state => ({
    users: state.users
});

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            getData
        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(ItemUser);


