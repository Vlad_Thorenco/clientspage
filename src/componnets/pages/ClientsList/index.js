import React, {Component} from 'react';
import {filterName} from '../../../module/users'
import ItemUser from "../ItemUser";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

class ClientsList extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        // const {users} = this.props;
        return (
            <app-users-container>
                <input type="text"
                       ref={(node) => this.inputName = node}
                       onChange={(e) => this.props.filterName(e.target.value.toLowerCase())}
                       className='search-user'
                />
                <div className='clients-all-list'>
                    <ul>
                        {
                            this.props.users.filterUser === null
                            ? this.props.users.users.map((elem, index) => (
                                <ItemUser  key={index}
                                           name={elem.general}
                                           job={elem.job}
                                           avatar={elem.general}
                                           id={elem.general.id}
                                           getUserData={this.props.getUserData}
                                />
                            ))
                                : this.props.users.filterUser.map((elem, index) => (
                                    <ItemUser  key={index}
                                               name={elem.general}
                                               job={elem.job}
                                               avatar={elem.general}
                                               id={elem.general.id}
                                               getUserData={this.props.getUserData}
                                    /> ))
                        }
                    </ul>
                </div>
            </app-users-container>
        );
    }
}

const mapStateToProps = state => ({
    users: state.users
});

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            filterName
        },
        dispatch
    );


export default connect(mapStateToProps, mapDispatchToProps)(ClientsList);