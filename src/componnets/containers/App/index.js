import React, {Component} from "react";
import ClientsList from '../../pages/ClientsList/index';
import ActiveUser from "../../pages/ActiveUser";
import '../../../css/style.css';
import {bindActionCreators} from "redux";
import {getUsers} from "../../../module/users";
import {connect} from "react-redux";


class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            obgInfo: {}
        }
    }

    componentWillMount() {
        this.props.getUsers();
    }

    render() {
        // const showMain = this.state.isShowComponent ? <main><ActiveUser info={this.props.users.activeData}/> </main> : null;
        return (
            <app-container>
                <div className='app'>
                    <aside>
                        <ClientsList users={this.props.users.users} getUserData={this.getUserData}/>
                    </aside>
                    {
                        this.props.users.activeData !== null
                        ? <main><ActiveUser info={this.props.users.activeData}/> </main>
                            : null
                    }
                </div>
            </app-container>
        )
    }
}


const mapStateToProps = state => ({
    users: state.users
});

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            getUsers
        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(App);