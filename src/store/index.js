import {createStore, applyMiddleware} from 'redux';
import reducer from '../module/index';
import thunk from 'redux-thunk';

// Метод redux который создает Store, вызывае 1 раз при анациализации
// reducer - аргумент
const store =  createStore(
    reducer,
    applyMiddleware(thunk)
);

export default store;

/*
applyMiddleware, thunk - не знаю что это такое
*/